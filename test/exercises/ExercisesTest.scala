package exercises

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person

class ExercisesTest {
  import exercises.Exercises.List._
  val lst = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testDrop() {
    assertEquals(drop(lst, 1), Cons(20, Cons(30, Nil())))
    assertEquals(drop(lst, 2), Cons(30, Nil()))
    assertEquals(drop(lst, 5), Nil())
  }

  @Test def testFlatMap() {
    assertEquals(flatMap( lst )(v => Cons ( v +1 , Nil () )) , Cons (11 , Cons (21 , Cons (31 , Nil ()))))
    assertEquals(flatMap ( lst )(v => Cons ( v +1 , Cons (v +2 , Nil () ))), Cons (11 , Cons (12 , Cons (21 , Cons (22 , Cons (31 , Cons (32 , Nil ()))))))   )
  }

  @Test def testMapInTermsOfFlatMap(): Unit ={
    //non composite mapping
    assertEquals(
      mapInTermsOfFlatMap( lst )(v => 2),
      map( lst )(v => 2))

    //composite mapping
    assertEquals(
      mapInTermsOfFlatMap( lst )(v => Cons( v +1 , Cons (v +2 , Nil () ))),
      map( lst )(v => Cons ( v +1 , Cons (v +2 , Nil () )))  )
  }

  @Test def testFilterInTermsOfFlatMap(): Unit = {
    assertEquals(
      filter[Int](lst)(_ >=20), // 20,30
      filterInTermsOfFlatMap[Int](lst)(_ >=20) );
  }

  import u02.Optionals.Option._
  @Test def testMax(): Unit ={
    assertEquals(max( Cons(10 , Cons(25 , Cons(20 , Nil() ) ) ) ), Some(25))
    assertEquals(max( Nil()), None())
  }


  import u02.Modules.Person._
  import exercises.Exercises.List
  import exercises.Exercises._

  //must force typing to List[Person]
  val teachers: List[Person]  = Cons(Student("s1", 1), Cons(Teacher("t1", "history"), Cons(Teacher("t2", "math"), Cons(Teacher("t3", "english"), Nil()))))
  val students: List[Person] = Cons(Student("s1", 1), Cons(Student("s2", 1), Nil()))
  @Test def testCoursesTaughtBy(): Unit = {
    val courses = Cons("history", Cons("math", Cons("english", Nil())))
    assertEquals(
      coursesTaughtBy(teachers),
      courses
    )

    assertEquals(
      coursesTaughtBy(students),
      Nil()
    )
  }

  val lst3 = Cons (3 , Cons (7 , Cons (1 , Cons (5 , Nil () ) ) ) )
  @Test def testFoldRight(): Unit = {
    assertEquals(
      foldRight( lst3)( 0) ((x:Int, y:Int)=> x-y),   //foldLeft( lst3, 0, _-_),
        -8
    )
  }

  @Test def testFoldLeft(): Unit = {
    assertEquals(foldLeft(lst3)(0)(_-_),
      -16)

  }

  @Test def testDropStreams(): Unit = {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    assertEquals(
      Stream.toList(Stream.drop(s)(6) ),
      Cons (6 , Cons (7 , Cons (8 , Cons (9 , Nil ()))))
    )
  }

  @Test def testConstant(): Unit ={
    assertEquals(
      Stream.toList( Stream.take(Stream.constant("x"))(5) ),
      Cons ("x", Cons ("x", Cons ("x", Cons ("x", Cons ("x", Nil ())))))
    )
  }


  @Test def testFib():Unit = {
    val fibs : Stream [Int ] = generateFibonacci()
    assertEquals(Stream.toList(Stream.take(fibs)(8)),
      Cons (0 , Cons (1 , Cons (1 , Cons (2 , Cons (3 , Cons (5 , Cons (8 ,
        Cons (13 , Nil ())))))))))
  }

}