package exercises

import exercises.Exercises.List.{Cons, Nil, asList, flatMap}
object Exercises extends App {

  //es 1
  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }

    //new methods added to list

    //1.a
    def drop[A](l: List[A], n: Int): List[A] = l match { //n tells how many items to delete
      case Cons(h, t) if n == 0 => Cons(h, t)     //base case
      case Cons(h, t) if n > 0 => drop(t, n - 1)
      case Nil() => Nil()                         //base case
    }

    //es 1.b
    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case Nil() => Nil()   //base case
    }

    //es 1.c
    def asList[A](e: A): List[A] = Cons(e, Nil()) //create list of 1 element

    def mapInTermsOfFlatMap[A, B](l: List[A])(mapper: A => B): List[B] = {
      flatMap(l)(x=>asList(mapper(x)))
    }

    //es. 1.d
    def filterInTermsOfFlatMap[A](l1: List[A])(pred: A => Boolean): List[A] = {
      flatMap (l1) (_ match {
        case x if pred(x) => asList(x)
        case _ => Nil()
      })
    }

    import u02.Optionals.Option

    //es 2.
    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h, t) if t == Nil() => Option.Some(h) //base case 1
      case Nil() => Option.None()                     //base case 2

      case Cons(h, t) if h > Option.getOrElse(max(t), h) => Option.Some(h)  //recursive call 1 (not tail recursive) - never returning Empty - so orElse never used
      case Cons(h, t) => max(t)                                             //recursive call 2
    }
  }

  //es 3.
  import u02.Modules._
  import u02.Modules.Person.Teacher

  def coursesTaughtBy(teachers: List[Person]) : List[String] =
    flatMap(teachers)(_ match {
      case Teacher(name, course) => asList(course)
      case _ => Nil()
    })

  //es 4.
  def foldRight[A, B](l: List[A])(defaultValue: B)(binaryOp : (A,  B) => B): B  = l match {
    case Nil() => defaultValue                                            //base case
    case Cons(h, t) => binaryOp(h, foldRight(t)(defaultValue)(binaryOp))  //recursive call
  }

  def foldLeft[A, B](l: List[A]) (defaultValue: B)(binaryOp : (B, A) => B): B = l match {
    case Nil() => defaultValue                                          //base case
    case Cons(h, t) => foldLeft(t)(binaryOp(defaultValue, h))(binaryOp) //recursive call
  }

  sealed trait Stream[A]
  object Stream {

    private case class Empty[A]() extends Stream[A]

    private case class Cons[A](head: () => A, tail: () => Stream[A]) extends Stream[A]

    def empty[A](): Stream[A] = Empty()

    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }

    def toList[A](stream: Stream[A]): List[A] = stream match {
      case Cons(h, t) => List.Cons(h(), toList(t()))
      case _ => List.Nil()
    }

    def map[A, B](stream: Stream[A])(f: A => B): Stream[B] = stream match {
      case Cons(head, tail) => cons(f(head()), map(tail())(f))
      case _ => Empty()
    }

    def filter[A](stream: Stream[A])(pred: A => Boolean): Stream[A] = stream match {
      case Cons(head, tail) if (pred(head())) => cons(head(), filter(tail())(pred))
      case Cons(head, tail) => filter(tail())(pred)
      case _ => Empty()
    }

    def take[A](stream: Stream[A])(n: Int): Stream[A] = (stream, n) match {
      case (Cons(head, tail), n) if n > 0 => cons(head(), take(tail())(n - 1))
      case _ => Empty()
    }

    def iterate[A](init: => A)(next: A => A): Stream[A] = cons(init, iterate(next(init))(next))

    //new methods added to Stream
    //es 5.
    def drop[A](s: Stream[A])(n: Int): Stream[A] = s match {
      case Cons(h, t) if n == 0 => Cons(h, t)         //base case
      case Empty() => Empty()                         //base case
      case Cons(h, t) /*if n>0*/ => drop(t())(n - 1)  //rec call
    }

    //es 6.
    def constant[A](k: A): Stream[A] = iterate(k)(identity)
  }

  //es 7.
  private def fibonacci(n:Int) : Int =  n match {
    case 0 | 1  => n
    case n if n > 1 => fibonacci(n-1) + fibonacci(n-2)
  }
  def generateFibonacci() : Stream[Int] = Stream.map(Stream.iterate(0)(_+1))(fibonacci)

}

